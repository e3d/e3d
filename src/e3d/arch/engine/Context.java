package e3d.arch.engine;

import e3d.arch.engine.camera.Camera;
import e3d.arch.engine.camera.CameraManager;
import e3d.arch.engine.console.Stats;
import e3d.arch.engine.material.MaterialManager;
import e3d.arch.engine.model.IModel;
import e3d.arch.engine.render.IRenderInitializer;
import e3d.arch.engine.render.IRenderProvider;
import e3d.arch.engine.render.RealRender;
import e3d.arch.engine.shader.ShaderManager;
import e3d.arch.engine.texture.TextureManager;
import e3d.arch.geometry.Matrix;

public class Context {

	private int unqiueIdentifierCounter = 0;

	private int width;

	private int height;

	private int depth = 255;

	private Viewport viewport;

	private ZBuffer zBuffer;

	private CameraManager cameraManager;

	private TextureManager textureManager;

	private ShaderManager shaderManager;

	private MaterialManager materialManager;

	private RealRender render;

	private Graphics graphics;

	private Matrix projectionDotModelView;

	private Matrix matrixZ;

	private Matrix invertedAndTranspozedProjectionDorModelView;

	private Matrix projection;

	private IRenderProvider renderProvider;

	private IRenderInitializer renderInitializer;

	private Stats stats;

	private IModel paintedModel;

	public Context(IRenderProvider renderProvider,
			IRenderInitializer renderInitializer) {
		this.renderProvider = renderProvider;
		this.renderInitializer = renderInitializer;
	}

	public Stats getStats() {
		if (stats == null) {
			stats = new Stats(this);
		}
		return stats;
	}

	public int getDepth() {
		return depth;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Graphics getGraphics() {
		if (graphics == null) {
			graphics = new Graphics(this);
		}
		return graphics;
	}

	public RealRender getRender() {
		if (render == null) {
			render = renderProvider.getRender(this, renderInitializer);
		}
		return render;
	}

	public Viewport getViewport() {
		if (viewport == null) {
			viewport = new Viewport(this);
		}
		return viewport;
	}

	public ZBuffer getZBuffer() {
		if (zBuffer == null) {
			zBuffer = new ZBuffer(this);
		}
		return zBuffer;
	}

	public MaterialManager getMaterialManager() {
		if (materialManager == null) {
			materialManager = new MaterialManager(this);
		}
		return materialManager;
	}

	public CameraManager getCameraManager() {
		if (cameraManager == null) {
			cameraManager = new CameraManager(this);
		}
		return cameraManager;
	}

	public TextureManager getTextureManager() {
		if (textureManager == null) {
			textureManager = new TextureManager(this);
		}
		return textureManager;
	}

	public ShaderManager getShaderManager() {
		if (shaderManager == null) {
			shaderManager = new ShaderManager(this);
		}
		return shaderManager;
	}

	public Matrix getProjection() {
		if (projection == null) {
			projection = Matrix.identity(4);
			Camera camera = getCameraManager().getActiveCamera();
			projection.values[3][2] = -1.f
					/ (camera.eye.minus(camera.center)).norm();
		}
		return projection;
	}

	public void setSize(int width, int height) {
		boolean needUpdate = false;
		if (this.width != width || this.height != height) {
			needUpdate = true;
		}
		this.width = width;
		this.height = height;
		if (needUpdate)
			update();
	}

	public void update() {
		projectionDotModelView = null;
		matrixZ = null;
		invertedAndTranspozedProjectionDorModelView = null;
		getViewport().update();
		getZBuffer().update(); // only if viewport size expanded
		getModelView().update();
	}

	public int generateUniqueIdentifier() {
		return unqiueIdentifierCounter++;
	}

	public void setNativeGraphics(java.awt.Graphics graphics) {
		Graphics artGraphics = getGraphics();
		artGraphics.setHeight(getHeight());
		artGraphics.setGraphics(graphics);
	}

	public Matrix getProjectionDotModelView() {
		if (projectionDotModelView == null) {
			projectionDotModelView = getProjection().product(getModelView());
		}
		return projectionDotModelView;
	}

	public Matrix getZ() {
		if (matrixZ == null) {
			matrixZ = getViewport().product(getProjectionDotModelView());
		}
		return matrixZ;
	}

	public Matrix getInvertedAndTranspozedProjectionDotModelView() {
		if (invertedAndTranspozedProjectionDorModelView == null) {
			invertedAndTranspozedProjectionDorModelView = getProjectionDotModelView()
					.invert_transpoze();
		}
		return invertedAndTranspozedProjectionDorModelView;
	}

	private Camera getModelView() {
		return getCameraManager().getActiveCamera();
	}

	public void beginPaintModel(IModel model) {
		this.paintedModel = model;
		getStats().paintModel(paintedModel);
	}

	public void endPaintModel() {
		getStats().endPaintModel(paintedModel);
		paintedModel = null;
	}

}
