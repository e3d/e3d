package e3d.arch.engine;

import java.awt.Color;

import e3d.arch.engine.model.Color3f;

public class Graphics extends ContextProvider {

	public java.awt.Graphics graphics;

	public boolean invertY = true;

	public int height;

	public Graphics(Context context) {
		setContext(context);
	}

	public java.awt.Graphics getGraphics() {
		return graphics;
	}

	public void setGraphics(java.awt.Graphics graphics) {
		this.graphics = graphics;
	}

	public void drawLine(float x1, float y1, float x2, float y2) {
		graphics.drawLine((int) x1, (int) (invertY ? (height - y1) : y1),
				(int) x2, (int) (invertY ? (height - y2) : y2));
	}

	public void set(int x, int y, Color3f color) {
		graphics.setColor(new Color(color.r, color.g, color.b));
		graphics.fillRect(x, invertY ? (height - y) : y, 1, 1);
	}

	public void setColor(Color3f color) {
		graphics.setColor(new Color(color.r, color.g, color.b));
	}

	public void setColor(float r, float g, float b) {
		graphics.setColor(new Color(r, g, b));
	}

	public void fillRect(int i, int j, int width, int height) {
		graphics.fillRect(i, j, width, height);
	}

	public void string(String string, int x, int y) {
		graphics.drawString(string, x, invertY ? (height - y) : y);
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
