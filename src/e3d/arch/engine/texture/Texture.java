package e3d.arch.engine.texture;

import java.awt.image.BufferedImage;

import e3d.arch.common.Utils;
import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.model.Color3f;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;

public class Texture extends ContextProvider {

	private int id;

	private BufferedImage image;

	private String file;

	public Texture(Context context) {
		this(context.generateUniqueIdentifier(), context);
	}

	public Texture(int id, Context context) {
		setContext(context);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getWidth() {
		return image.getWidth();
	}

	public int getHeight() {
		return image.getHeight();
	}

	public void setFile(String file) {
		this.file = file;
		load();
	}

	public String getFile() {
		return file;
	}

	private static void fillTextureData(Texture texture, String path) {
		if (path.endsWith(".tga")) {
			BufferedImage image = Utils.readAndFlipTGAImage(path);
			texture.image = image;
			texture.file = path;
		} else if (path.endsWith(".jpg")) {
			BufferedImage image = Utils.readAndFlipJPGImage(path);
			texture.image = image;
			texture.file = path;
		} else
			throw new UnsupportedOperationException("Can't load texture: \""
					+ path + "\"");
	}

	private void load() {
		fillTextureData(this, file);
	}

	public static Texture loadFromFile(Context context, String path) {
		Texture texture = new Texture(context);
		fillTextureData(texture, path);
		return texture;
	}

	public Color3f getColor(Vec2f uv) {
		int colorIndex = image.getRGB((int) (uv.x * (getWidth() - 1)),
				(int) (uv.y * (getHeight() - 1)));
		return new Color3f(Utils.bluef(colorIndex), Utils.greenf(colorIndex),
				Utils.redf(colorIndex));
	}

	public Vec3f getNormal(Vec2f uv) {
		int colorIndex = image.getRGB((int) (uv.x * (getWidth() - 1)),
				(int) (uv.y * (getHeight() - 1)));
		return new Vec3f(Utils.bluef(colorIndex), Utils.greenf(colorIndex),
				Utils.redf(colorIndex));
	}

	public int getSpeci(Vec2f uv) {
		int colorIndex = image.getRGB((int) (uv.x * (getWidth() - 1)),
				(int) (uv.y * (getHeight() - 1)));
		return Utils.bluei(colorIndex);
	}

	public float getSpecf(Vec2f uv) {
		int colorIndex = image.getRGB((int) (uv.x * (getWidth() - 1)),
				(int) (uv.y * (getHeight() - 1)));
		return Utils.bluef(colorIndex);
	}

}
