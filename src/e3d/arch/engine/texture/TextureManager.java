package e3d.arch.engine.texture;

import java.util.HashMap;
import java.util.Map;

import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;

public class TextureManager extends ContextProvider {

	private static int unqiueIdentifierCounter = 0;

	private Map<Integer, Texture> textures = new HashMap<Integer, Texture>();

	public TextureManager(Context context) {
		setContext(context);
	}

	public Texture loadTexture(String string) {
		return addTexture(Texture.loadFromFile(getContext(), string));
	}

	public Texture addTexture(Texture texture) {
		textures.put(texture.getId(), texture);
		return texture;
	}

	public static int generateUniqueIdentifier() {
		return unqiueIdentifierCounter++;
	}

	public Texture getTexture(int textureId) {
		return textures.get(textureId);
	}

	public Map<Integer, Texture> getTextures() {
		return textures;
	}

}
