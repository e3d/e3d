package e3d.arch.engine;

import e3d.arch.geometry.Matrix;

public class Viewport extends ContextProvider {

	Matrix matrix = new Matrix(4, 4);

	private float insets = 0;

	public Viewport(Context context) {
		setContext(context);
	}

	public void update() {
		float x = getX();
		float y = getY();
		float width = getWidth();
		float height = getHeight();
		matrix.values[3][3] = 1;
		matrix.values[0][3] = x + width / 2.f;
		matrix.values[1][3] = y + height / 2.f;
		matrix.values[2][3] = 1f;
		matrix.values[0][0] = width / 2.f;
		matrix.values[1][1] = height / 2.f;
		matrix.values[2][2] = 0;
	}

	public float getX() {
		return insets;
	}

	public float getY() {
		return insets;
	}

	public float getWidth() {
		return ((float) getContext().getWidth()) - insets * 2f;
	}

	public float getHeight() {
		return ((float) getContext().getHeight()) - insets * 2f;
	}

	public Matrix productAndTranspoze(Matrix projection) {
		return matrix.productAndTranspoze(projection);
	}

	public Matrix product(Matrix projection) {
		return matrix.product(projection);
	}

}
