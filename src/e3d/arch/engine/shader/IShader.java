package e3d.arch.engine.shader;

import e3d.arch.engine.IContextProvider;
import e3d.arch.engine.material.IMaterial;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.texture.Texture;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public interface IShader extends IContextProvider {

	public String getName();

	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir);

	public boolean fragment(Model model, Vec3f bar, Color3f color);

	public void setDiffuseMap(Texture texture);

	public void setNormalMap(Texture texture);

	public void setSpecMap(Texture texture);

	public void setMaterial(IMaterial material);

}
