package e3d.arch.engine.shader.base;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public class TexturedGuroProjectionModelShader extends AbstractShader {

	public static final String SHADER_TEXTURED_GURO_PROJECTION = "Textured projection with guro toning model shader";
	
	private Vec3f varying_intensity = new Vec3f();
	
	private Matrix varying_uv = new Matrix(2, 3);
	
	public TexturedGuroProjectionModelShader(Context context) {
		super(context, SHADER_TEXTURED_GURO_PROJECTION);
	}

	@Override
	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir) {
		int[] indexes = model.faces.get(i)[j];
		Vec3f model_vertex = model.vertexes.get(indexes[Model.INDEX_VERTEX]);
		Vec3f model_normal = model.normals.get(indexes[Model.INDEX_NORMAL]);
		Vec2f model_uv = model.uvs.get(indexes[Model.INDEX_UV]);
		
        varying_uv.setColumn(j, model_uv);
		
		if (j == 0)
			varying_intensity.x = Math.max(0.f,	model_normal.dotProduct(lightDir));
		else if (j == 1)
			varying_intensity.y = Math.max(0.f, model_normal.dotProduct(lightDir));
		else if (j == 2)
			varying_intensity.z = Math.max(0.f, model_normal.dotProduct(lightDir));

        Vec4f gl_Vertex = getContext().getProjectionDotModelView().product(model_vertex.toMatrix4Singleton()).toVector4f();
        varying_tri.setColumn(j, gl_Vertex);
		return gl_Vertex;
	}

	@Override
	public boolean fragment(Model model, Vec3f bar, Color3f color) {
		float intensity = varying_intensity.dotProduct(bar);
		Vec2f uv = varying_uv.product(bar).vertToVector2F();
		Color3f textureColor = diffuseMap.getColor(uv);
    	color.r = textureColor.r*intensity;
    	color.g = textureColor.g*intensity;
    	color.b = textureColor.b*intensity;
		return false;
	}

}
