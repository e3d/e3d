package e3d.arch.engine.shader.base;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public class PolygonedProjectionModelGrayscaleShader extends AbstractShader {

	public static final String SHADER_POLYGONED_PROJECTION_MODEL_GRAYSCALE = "Polygoned model with projection with grayscale colors";

	private Vec3f world_coords[] = new Vec3f[3];
	
	private Vec3f lightDir;

	public PolygonedProjectionModelGrayscaleShader(Context context) {
		super(context, SHADER_POLYGONED_PROJECTION_MODEL_GRAYSCALE);
	}

	@Override
	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir) {
		world_coords[j] = model.vertexes.get(model.faces.get(i)[j][Model.INDEX_VERTEX]);
		this.lightDir = lightDir;
        Vec4f gl_Vertex = getContext().getProjectionDotModelView().product(world_coords[j].toMatrix4Singleton()).toVector4f();
        varying_tri.setColumn(j, gl_Vertex);
		return gl_Vertex;
	}

	@Override
	public boolean fragment(Model model, Vec3f bar, Color3f color) {
		Vec3f n = world_coords[2].minus(world_coords[0]).crossProduct(world_coords[1].minus(world_coords[0]));
		float intensity = n.normalize().dotProduct(lightDir);
		color.r = intensity < 0 ? -intensity : intensity;
		color.g = intensity < 0 ? -intensity : intensity;
		color.b = intensity < 0 ? -intensity : intensity;
		return false;
	}

}
