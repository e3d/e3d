package e3d.arch.engine.shader.base;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public class GuroProjectionTexturedWithNormalMapModelShader extends AbstractShader {

	public static final String SHADER_GURO_PROJECTION_TEXTURED_NOTMAL_MAP = "Shader with guro toning for textured model with normal map";
	
	private Matrix varying_uv = new Matrix(2, 3);
	
	private Vec3f lightDir;
	
	public GuroProjectionTexturedWithNormalMapModelShader(Context context) {
		super(context, SHADER_GURO_PROJECTION_TEXTURED_NOTMAL_MAP);
	}

	@Override
	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir) {
		int[] indexes = model.faces.get(i)[j];
		Vec3f model_vertex = model.vertexes.get(indexes[Model.INDEX_VERTEX]);
		Vec2f model_uv = model.uvs.get(indexes[Model.INDEX_UV]);
		this.lightDir = lightDir;
        varying_uv.setColumn(j, model_uv);
        Vec4f gl_Vertex = getContext().getProjectionDotModelView().product(model_vertex.toMatrix4Singleton()).toVector4f();
        varying_tri.setColumn(j, gl_Vertex);
		return gl_Vertex;
	}

	@Override
	public boolean fragment(Model model, Vec3f bar, Color3f color) {		
		Vec2f uv = varying_uv.product(bar).vertToVector2F();
		Vec3f texture_normal = normalMap.getNormal(uv);		
		Vec3f n = getContext().getInvertedAndTranspozedProjectionDotModelView().product(texture_normal.toVec4f().toMatrixSingleton()).toVector4f().toVec3f().normalize();
		Vec3f l = getContext().getProjectionDotModelView()                     .product(lightDir      .toVec4f().toMatrixSingleton()).toVector4f().toVec3f().normalize();
		float nl = n.dotProduct(l);
		float intensity = nl > 0 ? nl : 0;
		Color3f textureColor = diffuseMap.getColor(uv);
    	color.r = textureColor.r*intensity;
    	color.g = textureColor.g*intensity;
    	color.b = textureColor.b*intensity;
		return false;
	}

}
