package e3d.arch.engine.shader.base;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public class TexturedPolygonedProjectionModelShader extends AbstractShader {

	public static final String SHADER_TEXTURED_POLYGONED_PROJECTION = "Textured projection with polygoned toning model shader";
	
	private Vec3f world_coords[] = new Vec3f[3];
	
	private Matrix varying_uv = new Matrix(2, 3);
	
	private Vec3f lightDir;
	
	public TexturedPolygonedProjectionModelShader(Context context) {
		super(context, SHADER_TEXTURED_POLYGONED_PROJECTION);
	}

	@Override
	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir) {
		int[] indexes = model.faces.get(i)[j];
		Vec2f model_uv = model.uvs.get(indexes[Model.INDEX_UV]);
		this.lightDir = lightDir;
        varying_uv.setColumn(j, model_uv);
        world_coords[j] = model.vertexes.get(model.faces.get(i)[j][Model.INDEX_VERTEX]);
        
        Vec4f gl_Vertex = getContext().getProjectionDotModelView().product(world_coords[j].toMatrix4Singleton()).toVector4f();
        varying_tri.setColumn(j, gl_Vertex);
		return gl_Vertex;
	}

	@Override
	public boolean fragment(Model model, Vec3f bar, Color3f color) {
		Vec3f n = world_coords[2].minus(world_coords[0]).crossProduct(world_coords[1].minus(world_coords[0]));
		float intensity = n.normalize().dotProduct(lightDir);
		intensity = intensity < 0 ? -intensity : intensity;
		Vec2f uv = varying_uv.product(bar).vertToVector2F();
		Color3f textureColor = diffuseMap.getColor(uv);
    	color.r = textureColor.r*intensity;
    	color.g = textureColor.g*intensity;
    	color.b = textureColor.b*intensity;
		return false;
	}

}
