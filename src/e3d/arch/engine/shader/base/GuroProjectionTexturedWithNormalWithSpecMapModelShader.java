package e3d.arch.engine.shader.base;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;
import e3d.arch.geometry.Vec4f;

public class GuroProjectionTexturedWithNormalWithSpecMapModelShader extends AbstractShader {

	public static final String SHADER_GURO_PROJECTION_TEXTURED_NORMAL_MAP_SPEC = "Shader with guro toning for textured model with normal map with spec";
	
	private Matrix varying_uv = new Matrix(2, 3);
	
	private Vec3f lightDir;
	
	public GuroProjectionTexturedWithNormalWithSpecMapModelShader(Context context) {
		super(context, SHADER_GURO_PROJECTION_TEXTURED_NORMAL_MAP_SPEC);
	}

	@Override
	public Vec4f vertex(Model model, int i, int j, Vec3f lightDir) {
		int[] indexes = model.faces.get(i)[j];
		Vec3f model_vertex = model.vertexes.get(indexes[Model.INDEX_VERTEX]);
		Vec2f model_uv = model.uvs.get(indexes[Model.INDEX_UV]);
		this.lightDir = lightDir;
        varying_uv.setColumn(j, model_uv);
        Vec4f gl_Vertex = getContext().getProjectionDotModelView().product(model_vertex.toMatrix4Singleton()).toVector4f();
        varying_tri.setColumn(j, gl_Vertex);
		return gl_Vertex;
	}

	@Override
	public boolean fragment(Model model, Vec3f bar, Color3f color) {		
		Vec2f uv = varying_uv.product(bar).vertToVector2F();
		if(uv.x < 0)
			return true; 
		if(uv.y < 0)
			return true;
		if(uv.x > 1)
			return true;
		if(uv.y > 1)
			return true;
		Vec3f texture_normal = normalMap.getNormal(uv);		
		Context context = getContext(); 
		Vec3f n = context.getInvertedAndTranspozedProjectionDotModelView().product(texture_normal.toVec4fSingleton().toMatrixSingleton()).toVector3f().normalize();
		Vec3f l = context.getProjectionDotModelView()                     .product(lightDir      .toVec4fSingleton().toMatrixSingleton()).toVector3f().normalize();
		
		float nl = n.dotProduct(l);
		Vec3f r = n.mul(nl*2f)._minus(l).normalize();
		float specPart = (float)Math.pow(r.z > 0 ? r.z : 0, specMap.getSpeci(uv));
				
		Color3f textureColor = diffuseMap.getColor(uv);
		
		float specular = specPart*material.getSpecular();
		float diffuse = (nl > 0 ? nl : 0)*material.getDiffuse();
		float ambient = material.getAmbient();
		
    	color.r = ambient + textureColor.r*(diffuse + specular);
    	color.r = color.r > 1 ? 1 : color.r;

    	color.g = ambient + textureColor.g*(diffuse + specular);
    	color.g = color.g > 1 ? 1 : color.g;

    	color.b = ambient + textureColor.b*(diffuse + specular);
    	color.b = color.b > 1 ? 1 : color.b;

		return false;
	}

}
