package e3d.arch.engine.shader;

import java.util.HashMap;
import java.util.Map;

import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.shader.base.GuroProjectionTexturedWithNormalWithSpecMapModelShader;

public class ShaderManager extends ContextProvider {

	public static final String SHADER_ROLE_DEFAULT = "Default shader role";

	private Map<String, IShader> shaders = new HashMap<String, IShader>();

	private Map<String, IShader> shadersByRole = new HashMap<String, IShader>();

	public ShaderManager(Context context) {
		setContext(context);
		addShaderByRole(SHADER_ROLE_DEFAULT,
				new GuroProjectionTexturedWithNormalWithSpecMapModelShader(
						context));
	}

	public void addShader(IShader shader) {
		shaders.put(shader.getName(), shader);
	}

	public void addShaderByRole(String role, IShader shader) {
		shadersByRole.put(role, shader);
	}

	public IShader getDefaultShader() {
		return shadersByRole.get(SHADER_ROLE_DEFAULT);
	}

	public IShader getShader(String shaderName) {
		return shaders.get(shaderName);
	}

	public Map<String, IShader> getShaders() {
		return shaders;
	}

	public void load(String shaderName) {
		throw new UnsupportedOperationException("Unimplemented yet!");
	}

}
