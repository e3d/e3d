package e3d.arch.engine.shader;

import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.material.IMaterial;
import e3d.arch.engine.texture.Texture;
import e3d.arch.geometry.Matrix;

public abstract class AbstractShader extends ContextProvider implements IShader {

	private String name;

	protected Texture diffuseMap;

	protected Texture normalMap;

	protected Texture specMap;

	protected IMaterial material;
	
	public Matrix varying_tri = new Matrix(4, 3);

	public AbstractShader(Context context, String name) {
		setContext(context);
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setDiffuseMap(Texture texture) {
		this.diffuseMap = texture;
	}

	@Override
	public void setNormalMap(Texture texture) {
		this.normalMap = texture;
	}

	@Override
	public void setSpecMap(Texture texture) {
		this.specMap = texture;
	}

	@Override
	public void setMaterial(IMaterial material) {
		this.material = material;
	}

}
