package e3d.arch.engine.render;

import e3d.arch.engine.Context;

public interface IRenderProvider {

	public RealRender getRender(Context context, IRenderInitializer renderInitializer);

}
