package e3d.arch.engine.render;

import e3d.arch.engine.Context;

public interface IRenderInitializer {

	public void init(Context context, AbstractRender abstractRender);

}
