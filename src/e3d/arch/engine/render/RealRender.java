package e3d.arch.engine.render;

import java.awt.Color;

import e3d.arch.engine.Context;
import e3d.arch.engine.Graphics;
import e3d.arch.engine.ZBuffer;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.shader.IShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;

public class RealRender extends AbstractRender {
	
	private Color3f color = new Color3f();
	
	private Color3f prevColor = new Color3f();
	
	public RealRender(Context context) {
		super(context);
	}

	@Override
	protected void triangle(Matrix clipc, IShader shader, ZBuffer zBuffer,
			Model model, Graphics graphics, int x, int y, int width,
			int height, int depth) {
		Matrix pts = getContext().getViewport().productAndTranspoze(clipc);
		Matrix pts2 = new Matrix(3, 2);
		for (int i=0; i<3; i++) {
			pts2.values[i][0] = pts.values[i][0]/pts.values[i][3];
			pts2.values[i][1] = pts.values[i][1]/pts.values[i][3];
		}
		float xMin = pts2.values[0][0];
		float xMax = pts2.values[0][0];
		float yMin = pts2.values[0][1];
		float yMax = pts2.values[0][1];
		for (int i = 1; i < 3; i++) {
			if (xMin > pts2.values[i][0])
				xMin = pts2.values[i][0];
			if (xMax < pts2.values[i][0])
				xMax = pts2.values[i][0];
			if (yMin > pts2.values[i][1])
				yMin = pts2.values[i][1];
			if (yMax < pts2.values[i][1])
				yMax = pts2.values[i][1];
		}
		if (xMax >= x + width)
			xMax = x + width - 1;
		if (xMin < x)
			xMin = x;
		if (yMax >= y + height)
			yMax = y + height - 1;
		if (yMin < y)
			yMin = y;
		Vec2f P = new Vec2f();
		for (P.x = xMin; P.x <= xMax; P.x++) {
			for (P.y = (int)yMin; P.y <= yMax; P.y++) {					
				Vec3f s1 = new Vec3f(pts2.values[2][1] - pts2.values[0][1], pts2.values[1][1] - pts2.values[0][1], pts2.values[0][1] - P.y);
				Vec3f s0 = new Vec3f(pts2.values[2][0] - pts2.values[0][0], pts2.values[1][0] - pts2.values[0][0], pts2.values[0][0] - P.x);
				Vec3f u = s0.crossProductSingleton(s1);
				Vec3f bc_screen = Math.abs(u.y) > 1e-2 ? new Vec3f(1.f - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z) : new Vec3f(-1, 1, 1);
				Vec3f bc_clip   = new Vec3f(bc_screen.x/pts.values[0][3], bc_screen.y/pts.values[1][3], bc_screen.z/pts.values[2][3]);
				bc_clip._div(bc_clip.x + bc_clip.y + bc_clip.z);
				float frag_depth = clipc.values[2][0]*bc_clip.x + clipc.values[2][1]*bc_clip.y +  clipc.values[2][2]*bc_clip.z;
				int zBufferIndex = (int)(P.x + P.y*width);
				if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0 || zBuffer.buffer[zBufferIndex] > frag_depth)
					continue;
				if (!shader.fragment(model, bc_clip, color)) {
					zBuffer.buffer[zBufferIndex] = frag_depth;
					if(prevColor.r != color.r && prevColor.g != color.g && prevColor.b != color.b) {
						graphics.graphics.setColor(new Color(color.r, color.g, color.b));
						prevColor.r = color.r;
						prevColor.g = color.g;
						prevColor.b = color.b;
					}
					graphics.graphics.fillRect((int)P.x, (int)(graphics.invertY ? (height - P.y) : P.y), 1, 1);
				}
			}
		}
	}

}
