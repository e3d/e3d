package e3d.arch.engine.render;

import e3d.arch.engine.Context;

public class InContextRenderProvider implements IRenderProvider {

	private RealRender render;

	@Override
	public RealRender getRender(Context context,
			IRenderInitializer renderInitializer) {
		if (render == null) {
			render = new RealRender(context);
			renderInitializer.init(context, render);
		}
		return render;
	}

}
