package e3d.arch.engine.render;

import java.util.List;

import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.Graphics;
import e3d.arch.engine.Viewport;
import e3d.arch.engine.ZBuffer;
import e3d.arch.engine.material.IMaterial;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.model.light.ILight;
import e3d.arch.engine.shader.AbstractShader;
import e3d.arch.engine.shader.IShader;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec3f;

public abstract class AbstractRender extends ContextProvider {

	public AbstractRender(Context context) {
		setContext(context);
	}

	public void render(Model model, List<ILight> lights) {
		List<int[][]> faces = model.getFaces();
		AbstractShader shader = (AbstractShader)getShader(model);
		IMaterial material = getMaterial(model);
		shader.setMaterial(material);
		Context context = getContext();
		int depth = context.getDepth();
		shader.setDiffuseMap(context.getTextureManager().getTexture(
				model.getDiffuseMapId()));
		shader.setNormalMap(context.getTextureManager().getTexture(
				model.getNormalMapId()));
		shader.setSpecMap(context.getTextureManager().getTexture(
				model.getSpecMapId()));
		Graphics graphics = context.getGraphics();
		Viewport viewport = context.getViewport();
		int width = (int) viewport.getWidth();
		int height = (int) viewport.getHeight();
		int x = (int) viewport.getX();
		int y = (int) viewport.getY();
		ZBuffer zBuffer = context.getZBuffer();
		Vec3f lightDir = lights.get(0).getDir().copy().normalize();
		for (int i = 0; i < faces.size(); i++) {
			for (int j = 0; j < 3; j++) {
				shader.vertex(model, i, j, lightDir);
			}
			triangle(shader.varying_tri, shader, zBuffer, model, graphics, x, y,
					width, height, depth);
		}
	}

	private IMaterial getMaterial(Model model) {
		String materialName = model.getMaterialName();
		if (materialName == null) {
			return getContext().getMaterialManager().getDefaultMaterial();
		}
		return getContext().getMaterialManager().getMaterial(materialName);
	}

	private IShader getShader(Model model) {
		String shaderName = model.getShaderName();
		if (shaderName == null) {
			return getContext().getShaderManager().getDefaultShader();
		}
		return getContext().getShaderManager().getShader(shaderName);
	}

	abstract protected void triangle(Matrix pts, IShader shader,
			ZBuffer zBuffer, Model model, Graphics graphics, int x, int y,
			int width, int height, int depth);

}
