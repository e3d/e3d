package e3d.arch.engine.material.base;

import e3d.arch.engine.material.Material;

public class DefaultMaterial extends Material {

	public static final String MATERIAL_DEFAULT = "Default material";

	public DefaultMaterial() {
		super(MATERIAL_DEFAULT, 5f / 255f, .1f, .8f);
	}

}
