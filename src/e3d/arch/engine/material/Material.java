package e3d.arch.engine.material;

public class Material implements IMaterial {

	private String name;
	
	private float ambient;

	private float specular;

	private float diffuse;

	public Material(String name) {
		this(name, 1, 1, 1);
	}

	public Material(String name, float ambient, float specular, float diffuse) {
		this.name = name;
		this.ambient = ambient;
		this.specular = specular;
		this.diffuse = diffuse;
	}

	public float getAmbient() {
		return ambient;
	}

	public float getSpecular() {
		return specular;
	}

	public float getDiffuse() {
		return diffuse;
	}

	public String getName() {
		return name;
	}

	public void setAmbient(float ambient) {
		this.ambient = ambient;
	}

	public void setSpecular(float specular) {
		this.specular = specular;
	}

	public void setDiffuse(float diffuse) {
		this.diffuse = diffuse;
	}	
	
}
