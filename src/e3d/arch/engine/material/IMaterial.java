package e3d.arch.engine.material;

public interface IMaterial {
	
	public String getName();
	
	public float getAmbient();
	
	public float getDiffuse();
	
	public float getSpecular();

}
