package e3d.arch.engine;

public class ZBuffer extends ContextProvider {

	public float[] buffer;

	public ZBuffer(Context context) {
		setContext(context);
	}

	public void update() {
		Context context = getContext();
		buffer = new float[context.getWidth() * context.getHeight()];
	}

	public void clean() {
		for (int i = 0, len = buffer.length; i < len; i++)
			buffer[i] = Float.MIN_VALUE;
	}

}
