package e3d.arch.engine;

public interface IContextProvider {

	public Context getContext();

	public void setContext(Context context);

}
