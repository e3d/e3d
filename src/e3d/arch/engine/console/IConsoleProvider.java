package e3d.arch.engine.console;

public interface IConsoleProvider {

	public Console createConsole();

}
