package e3d.arch.engine.console;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.ISceneInitializer;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.model.Scene;
import e3d.arch.engine.model.light.Light;
import e3d.arch.engine.render.InContextRenderProvider;

public class DefaultConsoleProvider implements IConsoleProvider {

	@Override
	public Console createConsole() {
		return new Console(new ISceneInitializer() {
			@Override
			public void init(Context context, Scene scene) {
				scene.addModel(new Model(context, "head.obj",
						"african_head_diffuse.tga", "african_head_nm.tga",
						"african_head_spec.tga"));
				scene.addLight(new Light(1, 1, 1));
				scene.setColor(0, 0, 0);
				context.getCameraManager().createAndSetActiveCamera(1, 1, 3, 0,
						0, 0, 0, 1, 0);
			}

			@Override
			public String getName() {
				return "head";
			}

		}, new InContextRenderProvider());
	}

}
