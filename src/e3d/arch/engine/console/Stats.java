package e3d.arch.engine.console;

import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.model.IModel;

public class Stats extends ContextProvider {

	private long startPaint = 0;

	private long endPaint;

	public Stats(Context context) {
		setContext(context);
	}

	public void beginPaint() {
		startPaint = System.currentTimeMillis();
	}

	public void endPaint() {
		endPaint = System.currentTimeMillis();
	}

	public int getFPS() {
		return (int) (1000. / (endPaint - startPaint));
	}

	public void paintModel(IModel paintedModel) {

	}

	public void endPaintModel(IModel paintedModel) {

	}

}
