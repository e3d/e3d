package e3d.arch.engine.console;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.ISceneInitializer;
import e3d.arch.engine.model.Scene;
import e3d.arch.engine.model.serialize.Serializer;
import e3d.arch.engine.render.InContextRenderProvider;

public class LoadableConsoleProvider implements IConsoleProvider {

	private String name;

	public LoadableConsoleProvider(String name) {
		this.name = name;
	}

	@Override
	public Console createConsole() {
		return new Console(new ISceneInitializer() {

			@Override
			public void init(Context context, Scene scene) {
				Serializer.deserializeFromXML(context, scene, name);
			}

			@Override
			public String getName() {
				return name;
			}
		}, new InContextRenderProvider());
	}
}
