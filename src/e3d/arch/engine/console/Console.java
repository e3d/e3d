package e3d.arch.engine.console;

import java.awt.Graphics;

import e3d.arch.app.Config;
import e3d.arch.common.Utils;
import e3d.arch.engine.Context;
import e3d.arch.engine.ContextProvider;
import e3d.arch.engine.camera.Camera;
import e3d.arch.engine.model.ISceneInitializer;
import e3d.arch.engine.model.Scene;
import e3d.arch.engine.model.serialize.Serializer;
import e3d.arch.engine.render.IRenderInitializer;
import e3d.arch.engine.render.IRenderProvider;
import e3d.arch.engine.render.StubRenderInitializer;

public class Console extends ContextProvider {

	private Config config;

	private int width;

	private int height;

	private Scene scene;

	private ISceneInitializer sceneInitializer;

	public Console(ISceneInitializer sceneInitializer,
			IRenderProvider renderProvider) {
		this(sceneInitializer, renderProvider, new StubRenderInitializer());
	}

	public Console(ISceneInitializer sceneInitializer,
			IRenderProvider renderProvider, IRenderInitializer renderInitializer) {
		setContext(new Context(renderProvider, renderInitializer));
		this.sceneInitializer = sceneInitializer;
		init();
	}

	protected void init() {
		scene = new Scene();
		scene.init(getContext(), sceneInitializer);
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public void setSize(int width, int height) {
		if (this.width != width || this.height != height) {
			this.width = width;
			this.height = height;

			int viewWidth = width;
			int viewHeight = height;
			getContext().setSize(viewWidth, viewHeight);
		}
	}

	public void paint(Graphics graphics) {
		Context context = getContext();
		Stats stats = context.getStats();
		stats.beginPaint();
		context.setNativeGraphics(graphics);
		context.getZBuffer().clean();
		scene.paint(context, scene.getLights());
		stats.endPaint();
		paintStats();
	}

	private void paintStats() {
		if (config.isStats()) {
			Context context = getContext();
			e3d.arch.engine.Graphics graphics = context.getGraphics();
			graphics.setColor(.8f, .8f, .8f);
			Stats stats = context.getStats();
			graphics.string("FPS: " + stats.getFPS(), 0, 0);
			graphics.string("camera: "
					+ context.getCameraManager().getActiveCamera(), 0, 20);
		}
	}

	public void tick() {
		scene.tick(getContext());
	}

	public void sendKey(int key) {
		if (key == Utils.KEY_UP) {
			Context context = getContext();
			context.getCameraManager().getActiveCamera().moveTF(0.1f);
			context.update();
		} else if (key == Utils.KEY_DOWN) {
			Context context = getContext();
			context.getCameraManager().getActiveCamera().moveTF(-0.1f);
			context.update();
		} else if (key == Utils.KEY_LEFT) {
			Context context = getContext();
			context.getCameraManager().getActiveCamera().moveLR(0.1f);
			context.update();
		} else if (key == Utils.KEY_RIGHT) {
			Context context = getContext();
			context.getCameraManager().getActiveCamera().moveLR(-0.1f);
			context.update();
		}
	}

	public void serialize() {
		Serializer.serializeToXML(getContext(), scene,
				sceneInitializer.getName());
	}

	public void sendMouseMove(int deltaX, int deltaY) {
		Context context = getContext();
		Camera camera = context.getCameraManager().getActiveCamera();
		camera.rotateYaw(deltaX / 1000.f);
		camera.rotatePitch(deltaY / 1000.f);
		context.update();
	}
}
