package e3d.arch.engine.model;

import java.util.List;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.light.ILight;

public interface IModel extends IEntity {

	public void paint(Context context, List<ILight> lights);
	
	public void tick(Context context);

}
