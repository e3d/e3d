package e3d.arch.engine.model;

import java.util.ArrayList;
import java.util.List;

import e3d.arch.engine.Context;
import e3d.arch.engine.Graphics;
import e3d.arch.engine.model.light.ILight;

public class Scene extends AbstractModel {

	private List<ILight> lights = new ArrayList<ILight>();

	private Color3f color = new Color3f();

	public void init(Context context, ISceneInitializer sceneInitializer) {
		sceneInitializer.init(context, this);
	}

	@Override
	public void tickCurrent(Context context) {
	}

	public void addLight(ILight light) {
		lights.add(light);
	}

	public List<ILight> getLights() {
		return lights;
	}

	protected void paintCurrent(Context context, List<ILight> lights) {
		Graphics graphics = context.getGraphics();
		graphics.setColor(color);
		graphics.fillRect(0, 0, context.getWidth(), context.getHeight());
	}

	public void setColor(float r, float g, float b) {
		color.r = r;
		color.g = g;
		color.b = b;
	}

	public void setColor(Color3f color) {
		setColor(color.r, color.g, color.b);
	}

	public Color3f getBgColor() {
		return color;
	}

}
