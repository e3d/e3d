package e3d.arch.engine.model;

import e3d.arch.engine.Context;

public interface ISceneInitializer {

	public String getName();

	public void init(Context context, Scene scene);

}
