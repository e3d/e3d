package e3d.arch.engine.model.light;

import e3d.arch.geometry.Vec3f;

public class Light implements ILight {

	public Vec3f direction = new Vec3f(0, 0, 0);

	public Light() {
		this(0, 0, 0);
	}

	public Light(float x, float y, float z) {
		direction.x = x;
		direction.y = y;
		direction.z = z;
	}

	@Override
	public Vec3f getDir() {
		return direction;
	}

	public void setDirection(Vec3f direction) {
		setDirection(direction.x, direction.y, direction.z);
	}

	public void setDirection(float x, float y, float z) {
		direction.x = x;
		direction.y = y;
		direction.z = z;
	}

}
