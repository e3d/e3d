package e3d.arch.engine.model.light;

import e3d.arch.engine.model.IEntity;
import e3d.arch.geometry.Vec3f;

public interface ILight extends IEntity {

	public Vec3f getDir();

}
