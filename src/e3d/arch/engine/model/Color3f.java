package e3d.arch.engine.model;

import e3d.arch.common.Utils;

public class Color3f {

	public float r;

	public float g;

	public float b;

	public Color3f() {
		this(0, 0, 0);
	}

	public Color3f(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public String toString() {
		return r + " " + g + " " + b;
	}

	public static Color3f parse(String line) {
		return Utils.parseColor3fFromString(line, " ");
	}

	public Color3f copy() {
		return new Color3f(r, g, b);
	}

}
