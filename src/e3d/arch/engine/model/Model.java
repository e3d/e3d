package e3d.arch.engine.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import e3d.arch.common.Utils;
import e3d.arch.engine.Context;
import e3d.arch.engine.material.IMaterial;
import e3d.arch.engine.model.light.ILight;
import e3d.arch.engine.shader.IShader;
import e3d.arch.engine.texture.Texture;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;

public class Model extends AbstractModel {

	public static final int INDEX_VERTEX = 0;

	public static final int INDEX_UV = INDEX_VERTEX + 1;

	public static final int INDEX_NORMAL = INDEX_UV + 1;

	public List<Vec3f> vertexes = new ArrayList<Vec3f>();

	public List<Vec2f> uvs = new ArrayList<Vec2f>();

	public List<Vec3f> normals = new ArrayList<Vec3f>();

	public List<int[][]> faces = new ArrayList<int[][]>();

	private int diffuseMapId = -1;

	private int normalMapId = -1;

	private int specMapId = -1;

	private Color3f color = new Color3f(1, 1, 1);

	private String shaderName;

	private String materialName;

	private String objFile;

	private Vec3f baseRotation = new Vec3f();

	private Vec3f baseLocation = new Vec3f();

	public Model() {
	}

	public Model(Context context, String objFilePath, String diffuseFilePath,
			String normalMapFilePath, String specMapFilePath) {
		this();
		fillFromObjFile(objFilePath);
		setDiffuseMap(context.getTextureManager().loadTexture(diffuseFilePath));
		setNormalMap(context.getTextureManager().loadTexture(normalMapFilePath));
		setSpecMap(context.getTextureManager().loadTexture(specMapFilePath));
	}

	public String getObjFile() {
		return objFile;
	}

	public void setShader(IShader shader) {
		setShaderName(shader.getName());
	}

	public void setShaderName(String shaderName) {
		this.shaderName = shaderName;
	}

	@Override
	public void paint(Context context, List<ILight> lights) {
		context.getRender().render(this, lights);
	}

	public void setNormalMap(Texture normalMap) {
		setNormalMapId(normalMap.getId());
	}

	public int getNormalMapId() {
		return normalMapId;
	}

	public void setDiffuseMapId(int id) {
		diffuseMapId = id;
	}

	public void setSpecMapId(int id) {
		specMapId = id;
	}

	public void setNormalMapId(int id) {
		normalMapId = id;
	}

	public void setDiffuseMap(Texture texture) {
		setDiffuseMapId(texture.getId());
	}

	public void setSpecMap(Texture texture) {
		setSpecMapId(texture.getId());
	}

	public int getDiffuseMapId() {
		return diffuseMapId;
	}

	public int getSpecMapId() {
		return specMapId;
	}

	public List<Vec3f> getVertexes() {
		return vertexes;
	}

	public List<int[][]> getFaces() {
		return faces;
	}

	public Color3f getColor() {
		return color;
	}

	public String getShaderName() {
		return shaderName;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterial(IMaterial material) {
		setMaterialName(material.getName());
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public void fillFromObjFile(String path) {
		fillFromObjFile(this, path);
	}

	public static void fillFromObjFile(Model model, String path) {
		model.objFile = path;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(Utils.OBJECTS_FOLDER
					+ File.separator + path));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("v ")) {
					model.vertexes.add(Utils.parseVec3fFromString(
							line.substring(2), " "));
				} else if (line.startsWith("vt ")) {
					model.uvs.add(Utils.parseVec2fFromString(line.substring(3),
							" "));
				} else if (line.startsWith("vn ")) {
					model.normals.add(Utils.parseVec3fFromString(
							line.substring(3), " "));
				} else if (line.startsWith("f ")) {
					String[] faceVertexesStr = line.substring(2).split(" ");
					int[][] face = new int[faceVertexesStr.length][3];
					for (int i = 0; i < faceVertexesStr.length; i++) {
						int index = faceVertexesStr[i].indexOf("/");
						if (index > 0) {
							face[i][0] = Integer.parseInt(faceVertexesStr[i]
									.substring(0, index)) - 1;
							int secondIndex = faceVertexesStr[i].indexOf("/",
									index + 1);
							if (secondIndex > index + 1) {
								face[i][1] = Integer
										.parseInt(faceVertexesStr[i].substring(
												index + 1, secondIndex)) - 1;
								face[i][2] = Integer
										.parseInt(faceVertexesStr[i]
												.substring(secondIndex + 1)) - 1;
							}
						} else {
							face[i][0] = Integer.parseInt(faceVertexesStr[i]) - 1;
						}
					}
					model.faces.add(face);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Vec3f getBaseRotation() {
		return baseRotation;
	}

	public void setBaseRotation(Vec3f baseRotation) {
		this.baseRotation = baseRotation;
		rotate(baseRotation);
	}

	public Vec3f getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(Vec3f baseLocation) {
		this.baseLocation = baseLocation;
		move(baseLocation);
	}

	public void move(Vec3f delta) {
		move(delta.x, delta.y, delta.z);
	}

	public void move(float deltaX, float deltaY, float deltaZ) {
		for (Vec3f vertex : vertexes) {
			vertex.x += deltaX;
			vertex.y += deltaY;
			vertex.z += deltaZ;
		}
	}

	public void rotate(Vec3f delta) {
		rotate(delta.x, delta.y, delta.z);
	}

	/*public void rotate(float alpha, float beta, float gamma) {
		if (alpha == 0 && beta == 0 && gamma == 0)
			return;
		Matrix eilerRotationMatrix = Utils.eilerRotationMatrix(alpha, beta,
				gamma);
		for (int i = 0; i < vertexes.size(); i++) {
			vertexes.set(i, eilerRotationMatrix.product(vertexes.get(i))
					.toVector3f());
		}
	}*/
	
	public void rotate(float alpha, float beta, float gamma) {
		Matrix rotX = Utils.getRotXMatrix(alpha);
		Matrix rotY = Utils.getRotYMatrix(beta);
		Matrix rotZ = Utils.getRotZMatrix(gamma);
		for (int i = 0; i < vertexes.size(); i++) {
			vertexes.set(i, rotZ.product(rotY.product(rotX.product(vertexes.get(i)))).toVector3f());
		}
		for (int i = 0; i < normals.size(); i++) {
			normals.set(i, rotZ.product(rotY.product(rotX.product(normals.get(i)))).toVector3f());
		}
	}

	public Model copy() {
		Model model = new Model();
		for (Vec3f vertex : vertexes) {
			model.vertexes.add(vertex.copy());
		}
		for (Vec2f uv : uvs) {
			model.uvs.add(uv.copy());
		}
		for (Vec3f normal : normals) {
			model.normals.add(normal.copy());
		}
		for (int[][] face : faces) {
			int[][] newFace = new int[face.length][3];
			for (int j = 0; j < face.length; j++) {
				newFace[j][0] = face[j][0];
				newFace[j][1] = face[j][1];
				newFace[j][2] = face[j][2];
			}
			model.faces.add(newFace);
		}
		model.diffuseMapId = diffuseMapId;
		model.normalMapId = normalMapId;
		model.specMapId = specMapId;
		model.color = color.copy();
		model.shaderName = shaderName;
		model.materialName = materialName;
		model.objFile = objFile;
		model.baseLocation = baseLocation.copy();
		model.baseRotation = baseRotation.copy();
		return model;
	}

}
