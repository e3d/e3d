package e3d.arch.engine.model;

import java.util.ArrayList;
import java.util.List;

import e3d.arch.engine.Context;
import e3d.arch.engine.model.light.ILight;

public class AbstractModel implements IModel {

	private List<IModel> models = new ArrayList<IModel>();

	@Override
	public void paint(Context context, List<ILight> lights) {
		context.beginPaintModel(this);
		paintCurrent(context, lights);
		context.endPaintModel();
		paintChildrens(context, lights);
	}

	protected void paintCurrent(Context context, List<ILight> lights) {
	}

	private void paintChildrens(Context context, List<ILight> lights) {
		for (IModel model : models) {
			model.paint(context, lights);
		}
	}

	public void addModel(IModel model) {
		models.add(model);
	}

	@Override
	public void tick(Context context) {
		tickChildrens(context);
		tickCurrent(context);
	}

	protected void tickCurrent(Context context) {

	}

	private void tickChildrens(Context context) {
		for (IModel model : models) {
			model.tick(context);
		}
	}

	public List<IModel> getChild() {
		return models;
	}

}
