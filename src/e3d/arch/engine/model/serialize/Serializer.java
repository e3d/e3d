package e3d.arch.engine.model.serialize;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import e3d.arch.common.Utils;
import e3d.arch.engine.Context;
import e3d.arch.engine.camera.Camera;
import e3d.arch.engine.camera.CameraManager;
import e3d.arch.engine.material.IMaterial;
import e3d.arch.engine.material.Material;
import e3d.arch.engine.material.MaterialManager;
import e3d.arch.engine.model.AbstractModel;
import e3d.arch.engine.model.Color3f;
import e3d.arch.engine.model.IModel;
import e3d.arch.engine.model.Model;
import e3d.arch.engine.model.Scene;
import e3d.arch.engine.model.light.ILight;
import e3d.arch.engine.model.light.Light;
import e3d.arch.engine.shader.IShader;
import e3d.arch.engine.shader.ShaderManager;
import e3d.arch.engine.texture.Texture;
import e3d.arch.engine.texture.TextureManager;
import e3d.arch.geometry.Vec3f;

public class Serializer {

	public static final String EL_MATERIALS = "materials";

	public static final String EL_MATERIAL = "material";

	public static final String EL_CAMERAS = "cameras";

	public static final String EL_CAMERA = "camera";

	public static final String EL_TEXTURES = "textures";

	public static final String EL_TEXTURE = "texture";

	public static final String EL_DIFFUSE = "diffuse";

	public static final String EL_AMBIENT = "ambient";

	public static final String EL_SPECULAR = "specular";

	public static final String EL_ID = "id";

	public static final String EL_EYE = "eye";

	public static final String EL_UP = "up";

	public static final String EL_CENTER = "center";

	public static final String EL_NAME = "name";

	public static final String EL_LOCATION = "location";

	public static final String EL_MODEL = "model";

	public static final String EL_OBJ = "obj";

	public static final String EL_SCENE = "scene";

	public static final String EL_BACKGROUND = "background";

	public static final String EL_CHILDS = "childs";

	public static final String EL_NORMAL = "normal";

	public static final String EL_SHADERS = "shaders";

	public static final String EL_SHADER = "shader";

	public static final String EL_LIGHTS = "lights";

	public static final String EL_LIGHT = "light";

	public static final String EL_E3D = "e3d";

	public static final String EL_DIRECTION = "direction";

	public static final String EL_ACTIVE = "active";

	public static final String EL_VALUE_TRUE = "true";

	public static final String EL_ROTATION = "rotation";

	public static final String EL_VALUE_BLOCK = "block";

	public static final String EL_TYPE = "type";

	public static final String EL_BLOCK = "block";

	public static void serializeToXML(Context context, Scene scene, String name) {
		try {
			String serializedString = serializeToXMLString(context, scene);
			Utils.saveScene(serializedString, name);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public static void deserializeFromXML(Context context, Scene scene,
			String path) {
		String sceneString = Utils.readFromSceneFile(path);
		deserializeFromXMLString(context, scene, sceneString);
	}

	public static void deserializeFromXMLString(Context context, Scene scene,
			String sceneString) {

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		try {
			DocumentBuilder documentBuilder = documentBuilderFactory
					.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(
					new StringReader(sceneString)));
			deserializeFromXML(context, scene, document.getDocumentElement());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void deserializeFromXML(Context context, Scene scene,
			Element documentElement) {
		NodeList nodeList = documentElement.getChildNodes();
		List<ILight> lights = Collections.emptyList();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_CAMERAS)) {
				deserializeCameras(context, node.getChildNodes());
			} else if (node.getNodeName().equalsIgnoreCase(EL_MATERIALS)) {
				deserializeMaterials(context, node.getChildNodes());
			} else if (node.getNodeName().equalsIgnoreCase(EL_LIGHTS)) {
				lights = deserializeLights(context, node.getChildNodes());
			} else if (node.getNodeName().equalsIgnoreCase(EL_TEXTURES)) {
				deserializeTextures(context, node.getChildNodes());
			} else if (node.getNodeName().equalsIgnoreCase(EL_SHADERS)) {
				deserializeShaders(context, node.getChildNodes());
			}
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_SCENE)) {
				NodeList childNodeList = node.getChildNodes();
				for (int j = 0; j < childNodeList.getLength(); j++) {
					Node childNode = childNodeList.item(j);
					if (childNode.getNodeName().equalsIgnoreCase(EL_BACKGROUND)) {
						scene.setColor(Color3f.parse(childNode.getTextContent()));
					} else if (childNode.getNodeName().equalsIgnoreCase(
							EL_CHILDS)) {
						deserializeChilds(context, (AbstractModel) scene,
								childNode.getChildNodes());
					}
				}
				for (ILight light : lights) {
					scene.addLight(light);
				}
				break;
			}
		}
	}

	private static void deserializeChilds(Context context,
			AbstractModel parent, NodeList nodeList) {
		Map<Integer, Model> blocks = new HashMap<Integer, Model>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_MODEL)) {
				Node blockNode = node.getAttributes().getNamedItem(EL_TYPE);
				if (blockNode != null
						&& blockNode.getNodeValue().equalsIgnoreCase(
								EL_VALUE_BLOCK)) {
					Model model = new Model();
					NodeList childNodes = node.getChildNodes();
					Vec3f location = null;
					Vec3f rotation = null;
					for (int j = 0; j < childNodes.getLength(); j++) {
						Node childNode = childNodes.item(j);
						if (childNode.getNodeName().equals(EL_CHILDS)) {
							deserializeChilds(context, parent, nodeList);
						} else if (childNode.getNodeName().equals(EL_LOCATION)) {
							location = Vec3f.parse(childNode.getTextContent());
						} else if (childNode.getNodeName().equals(EL_ROTATION)) {
							rotation = Vec3f.parse(childNode.getTextContent());
						} else if (childNode.getNodeName().equals(EL_OBJ)) {
							model.fillFromObjFile(childNode.getTextContent());
						} else if (childNode.getNodeName().equals(EL_DIFFUSE)) {
							model.setDiffuseMapId(Integer.parseInt(childNode
									.getAttributes().getNamedItem(EL_ID)
									.getNodeValue()));
						} else if (childNode.getNodeName().equals(EL_SPECULAR)) {
							model.setSpecMapId(Integer.parseInt(childNode
									.getAttributes().getNamedItem(EL_ID)
									.getNodeValue()));
						} else if (childNode.getNodeName().equals(EL_NORMAL)) {
							model.setNormalMapId(Integer.parseInt(childNode
									.getAttributes().getNamedItem(EL_ID)
									.getNodeValue()));
						} else if (childNode.getNodeName().equals(EL_SHADER)) {
							model.setShaderName(childNode.getTextContent());
						} else if (childNode.getNodeName().equals(EL_MATERIAL)) {
							model.setMaterialName(childNode.getTextContent());
						}
					}
					if (rotation != null)
						model.rotate(rotation);
					if (location != null)
						model.move(location);
					blocks.put(
							Integer.parseInt(node.getAttributes()
									.getNamedItem(EL_ID).getNodeValue()), model);
				}
			}
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_MODEL)) {
				Node blockNode = node.getAttributes().getNamedItem(EL_TYPE);
				if (blockNode != null)
					continue;
				Node blockExtNode = node.getAttributes().getNamedItem(EL_BLOCK);
				Model model = null;
				if (blockExtNode != null) {
					model = blocks.get(
							Integer.parseInt(blockExtNode.getNodeValue()))
							.copy();
				} else {
					model = new Model();
				}
				NodeList childNodes = node.getChildNodes();
				Vec3f location = null;
				Vec3f rotation = null;
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node childNode = childNodes.item(j);
					if (childNode.getNodeName().equals(EL_CHILDS)) {
						deserializeChilds(context, parent, nodeList);
					} else if (childNode.getNodeName().equals(EL_LOCATION)) {
						location = Vec3f.parse(childNode.getTextContent());
					} else if (childNode.getNodeName().equals(EL_ROTATION)) {
						rotation = Vec3f.parse(childNode.getTextContent());
					} else if (childNode.getNodeName().equals(EL_OBJ)) {
						model.fillFromObjFile(childNode.getTextContent());
					} else if (childNode.getNodeName().equals(EL_DIFFUSE)) {
						model.setDiffuseMapId(Integer.parseInt(childNode
								.getAttributes().getNamedItem(EL_ID)
								.getNodeValue()));
					} else if (childNode.getNodeName().equals(EL_SPECULAR)) {
						model.setSpecMapId(Integer.parseInt(childNode
								.getAttributes().getNamedItem(EL_ID)
								.getNodeValue()));
					} else if (childNode.getNodeName().equals(EL_NORMAL)) {
						model.setNormalMapId(Integer.parseInt(childNode
								.getAttributes().getNamedItem(EL_ID)
								.getNodeValue()));
					} else if (childNode.getNodeName().equals(EL_SHADER)) {
						model.setShaderName(childNode.getTextContent());
					} else if (childNode.getNodeName().equals(EL_MATERIAL)) {
						model.setMaterialName(childNode.getTextContent());
					}
				}
				if (rotation != null)
					model.rotate(rotation);
				if (location != null)
					model.move(location);
				parent.addModel(model);
			}
		}
	}

	private static String serializeToXMLString(Context context, Scene scene)
			throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		docBuilder = docFactory.newDocumentBuilder();
		Document document = docBuilder.newDocument();
		Element rootElement = document.createElement(EL_E3D);
		document.appendChild(rootElement);
		serializeToXML(document, rootElement, context, scene);
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "2");
		DOMSource source = new DOMSource(document);

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		transformer.transform(source, result);
		return writer.getBuffer().toString();
	}

	private static void serializeToXML(Document document, Element rootElement,
			Context context, Scene scene) {
		serializeCameras(document, rootElement, context, scene);
		serializeLights(document, rootElement, context, scene);
		serializeTextures(document, rootElement, context, scene);
		serializeMaterials(document, rootElement, context, scene);
		serializeShaders(document, rootElement, context, scene);
		serializeScene(document, rootElement, context, scene);
	}

	private static void serializeScene(Document document, Element rootElement,
			Context context, Scene scene) {
		Element element = document.createElement(EL_SCENE);
		rootElement.appendChild(element);
		Element bgEl = document.createElement(EL_BACKGROUND);
		element.appendChild(bgEl);
		bgEl.setTextContent(scene.getBgColor().toString());
		serializeChildModels(document, element, scene.getChild());
	}

	private static void serializeChildModels(Document document,
			Element parentElement, List<IModel> childs) {
		if (childs.isEmpty())
			return;
		Element element = document.createElement(EL_CHILDS);
		parentElement.appendChild(element);
		for (IModel model : childs) {
			if (model instanceof Model) {
				Element modelEl = document.createElement(EL_MODEL);
				element.appendChild(modelEl);

				Element locationEl = document.createElement(EL_LOCATION);
				modelEl.appendChild(locationEl);
				locationEl.setTextContent(((Model) model).getBaseLocation()
						.toString());

				Element rotationEl = document.createElement(EL_ROTATION);
				modelEl.appendChild(locationEl);
				rotationEl.setTextContent(((Model) model).getBaseRotation()
						.toString());

				if (((Model) model).getObjFile() != null) {
					Element objEl = document.createElement(EL_OBJ);
					modelEl.appendChild(objEl);
					objEl.setTextContent(((Model) model).getObjFile());
				}

				if (((Model) model).getDiffuseMapId() >= 0) {
					Element diffuseEl = document.createElement(EL_DIFFUSE);
					modelEl.appendChild(diffuseEl);
					diffuseEl.setAttribute(EL_ID,
							((Model) model).getDiffuseMapId() + "");
				}

				if (((Model) model).getSpecMapId() >= 0) {
					Element specEl = document.createElement(EL_SPECULAR);
					modelEl.appendChild(specEl);
					specEl.setAttribute(EL_ID, ((Model) model).getSpecMapId()
							+ "");
				}

				if (((Model) model).getNormalMapId() >= 0) {
					Element normalEl = document.createElement(EL_NORMAL);
					modelEl.appendChild(normalEl);
					normalEl.setAttribute(EL_ID,
							((Model) model).getNormalMapId() + "");
				}

				if (((Model) model).getShaderName() != null) {
					Element shaderEl = document.createElement(EL_SHADER);
					modelEl.appendChild(shaderEl);
					shaderEl.setTextContent(((Model) model).getShaderName());
				}

				if (((Model) model).getMaterialName() != null) {
					Element materialEl = document.createElement(EL_MATERIAL);
					modelEl.appendChild(materialEl);
					materialEl
							.setTextContent(((Model) model).getMaterialName());
				}

				serializeChildModels(document, element,
						((Model) model).getChild());
			}
		}
	}

	private static void deserializeShaders(Context context, NodeList nodeList) {
		ShaderManager shaderManager = context.getShaderManager();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_SHADER)) {
				shaderManager.load(node.getAttributes().getNamedItem(EL_NAME)
						.getNodeValue());
			}
		}
	}

	private static void serializeShaders(Document document,
			Element rootElement, Context context, Scene scene) {
		Map<String, IShader> shaders = context.getShaderManager().getShaders();
		if (shaders.isEmpty())
			return;
		Element element = document.createElement(EL_SHADERS);
		rootElement.appendChild(element);
		for (IShader shader : shaders.values()) {
			Element shaderEl = document.createElement(EL_SHADER);
			element.appendChild(shaderEl);
			shaderEl.setAttribute(EL_NAME, shader.getName());
		}
	}

	private static void deserializeTextures(Context context, NodeList nodeList) {
		TextureManager textureManager = context.getTextureManager();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_TEXTURE)) {
				Texture texture = new Texture(Integer.parseInt(node
						.getAttributes().getNamedItem(EL_ID).getNodeValue()),
						context);
				texture.setFile(node.getTextContent());
				textureManager.addTexture(texture);
			}
		}
	}

	private static void serializeTextures(Document document,
			Element rootElement, Context context, Scene scene) {
		Map<Integer, Texture> textures = context.getTextureManager()
				.getTextures();
		if (textures.isEmpty())
			return;
		Element element = document.createElement(EL_TEXTURES);
		rootElement.appendChild(element);
		for (Texture texture : textures.values()) {
			Element textureEl = document.createElement(EL_TEXTURE);
			element.appendChild(textureEl);
			textureEl.setTextContent(texture.getFile());
			textureEl.setAttribute(EL_ID, texture.getId() + "");
		}
	}

	private static List<ILight> deserializeLights(Context context,
			NodeList nodeList) {
		List<ILight> lights = new ArrayList<ILight>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_LIGHT)) {
				NodeList childNodes = node.getChildNodes();
				Light light = new Light();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node childNode = childNodes.item(j);
					if (childNode.getNodeName().equals(EL_DIRECTION)) {
						light.setDirection(Vec3f.parse(childNode
								.getTextContent()));
					}
				}
				lights.add(light);
			}
		}
		return lights;
	}

	private static void serializeLights(Document document, Element rootElement,
			Context context, Scene scene) {
		List<ILight> lights = scene.getLights();
		if (lights.isEmpty())
			return;
		Element element = document.createElement(EL_LIGHTS);
		rootElement.appendChild(element);
		for (ILight light : lights) {
			Element lightEl = document.createElement(EL_LIGHT);
			element.appendChild(lightEl);
			Element directionEl = document.createElement(EL_DIRECTION);
			lightEl.appendChild(directionEl);
			directionEl.setTextContent(light.getDir().toString());
		}
	}

	private static void deserializeCameras(Context context, NodeList nodeList) {
		CameraManager cameraManager = context.getCameraManager();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_CAMERA)) {
				Camera camera = new Camera(Integer.parseInt(node
						.getAttributes().getNamedItem(EL_ID).getNodeValue()),
						context);
				NodeList childNodes = node.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node childNode = childNodes.item(j);
					if (childNode.getNodeName().equals(EL_EYE)) {
						camera.setEye(Vec3f.parse(childNode.getTextContent()));
					} else if (childNode.getNodeName().equals(EL_CENTER)) {
						camera.setCenter(Vec3f.parse(childNode.getTextContent()));
					} else if (childNode.getNodeName().equals(EL_UP)) {
						camera.setUp(Vec3f.parse(childNode.getTextContent()));
					}
				}
				cameraManager.addCamera(camera);
				Node activeNode = node.getAttributes().getNamedItem(EL_ACTIVE);
				if (activeNode != null
						&& Boolean.parseBoolean(activeNode.getNodeValue())) {
					cameraManager.setActiveCamera(camera.getId());
				}
			}
		}
	}

	private static void serializeCameras(Document document,
			Element rootElement, Context context, Scene scene) {
		Map<Integer, Camera> cameras = context.getCameraManager().getCameras();
		if (cameras.size() == 0)
			return;
		Element element = document.createElement(EL_CAMERAS);
		rootElement.appendChild(element);
		for (Entry<Integer, Camera> entry : cameras.entrySet()) {
			Element entryEl = document.createElement(EL_CAMERA);
			element.appendChild(entryEl);
			entryEl.setAttribute(EL_ID, entry.getValue().getId() + "");

			Element eyeEl = document.createElement(EL_EYE);
			entryEl.appendChild(eyeEl);
			eyeEl.setTextContent(entry.getValue().eye.toString());

			Element centerEl = document.createElement(EL_CENTER);
			entryEl.appendChild(centerEl);
			centerEl.setTextContent(entry.getValue().center.toString());

			Element upEl = document.createElement(EL_UP);
			entryEl.appendChild(upEl);
			upEl.setTextContent(entry.getValue().up.toString());

			if (context.getCameraManager().getActiveCameraId() == entry
					.getValue().getId()) {
				entryEl.setAttribute(EL_ACTIVE, EL_VALUE_TRUE);
			}
		}
	}

	private static void deserializeMaterials(Context context, NodeList nodeList) {
		MaterialManager materialManager = context.getMaterialManager();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equalsIgnoreCase(EL_MATERIAL)) {
				Material material = new Material(node.getAttributes()
						.getNamedItem(EL_NAME).getNodeValue());
				NodeList childNodes = node.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node childNode = childNodes.item(j);
					if (childNode.getNodeName().equals(EL_AMBIENT)) {
						material.setAmbient(Float.parseFloat(childNode
								.getTextContent()));
					} else if (childNode.getNodeName().equals(EL_SPECULAR)) {
						material.setSpecular(Float.parseFloat(childNode
								.getTextContent()));
					} else if (childNode.getNodeName().equals(EL_DIFFUSE)) {
						material.setDiffuse(Float.parseFloat(childNode
								.getTextContent()));
					}
				}
				materialManager.addMaterial(material);
			}
		}
	}

	private static void serializeMaterials(Document document,
			Element rootElement, Context context, Scene scene) {
		Map<String, IMaterial> materials = context.getMaterialManager()
				.getMaterials();
		if (materials.size() == 0)
			return;
		Element element = document.createElement(EL_MATERIALS);
		rootElement.appendChild(element);
		for (Entry<String, IMaterial> entry : materials.entrySet()) {
			Element entryEl = document.createElement(EL_MATERIAL);
			element.appendChild(entryEl);
			element.setAttribute(EL_NAME, entry.getValue().getName() + "");

			Element diffuseEl = document.createElement(EL_DIFFUSE);
			entryEl.appendChild(diffuseEl);
			diffuseEl.setTextContent(entry.getValue().getDiffuse() + "");

			Element specularEl = document.createElement(EL_SPECULAR);
			entryEl.appendChild(specularEl);
			specularEl.setTextContent(entry.getValue().getSpecular() + "");

			Element ambientEl = document.createElement(EL_AMBIENT);
			entryEl.appendChild(ambientEl);
			ambientEl.setTextContent(entry.getValue().getAmbient() + "");
		}
	}
}
