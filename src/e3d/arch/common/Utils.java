package e3d.arch.common;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteOrder;

import e3d.arch.engine.model.Color3f;
import e3d.arch.geometry.Matrix;
import e3d.arch.geometry.Vec2f;
import e3d.arch.geometry.Vec3f;

public class Utils {

	public static final int KEY_UP = 38;

	public static final int KEY_DOWN = 40;

	public static final int KEY_LEFT = 37;

	public static final int KEY_RIGHT = 39;

	public final static String E3D_SCENE_EXTENSION = "e3d";

	public final static int RGBABytesPerPixel = 4;

	public final static int RGBBytesPerPixel = 3;

	private static final int redOffset, greenOffset, blueOffset;

	static {
		boolean big = ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN;
		redOffset = big ? 0 : 16;
		greenOffset = big ? 8 : 8;
		blueOffset = big ? 16 : 0;
	}

	public static final String RESOURCES_FOLDER_NAME = "res";

	public static final String OBJECTS_FOLDER_NAME = "objs";

	public static final String IMAGES_FOLDER_NAME = "images";

	public static final String PACKAGES_FOLDER_NAME = "packages";

	public static final String OBJECTS_FOLDER = RESOURCES_FOLDER_NAME
			+ File.separator + OBJECTS_FOLDER_NAME;

	public static final String IMAGES_FOLDER = RESOURCES_FOLDER_NAME
			+ File.separator + IMAGES_FOLDER_NAME;

	public static final String PACKAGES_FOLDER = RESOURCES_FOLDER_NAME
			+ File.separator + PACKAGES_FOLDER_NAME;

	public static int[] RGBToArray(int rgb) {
		return new int[] { redi(rgb), bluei(rgb), greeni(rgb) };
	}

	public static int redi(int rgb) {
		return rgb >> redOffset & 0xff;
	}

	public static int greeni(int rgb) {
		return rgb >> greenOffset & 0xff;
	}

	public static int bluei(int rgb) {
		return rgb >> blueOffset & 0xff;
	}

	public static String readFromObjFile(String path) {
		return readFromFile(OBJECTS_FOLDER + File.separator + path);
	}

	public static String readFromSceneFile(String path) {
		return readFromFile(PACKAGES_FOLDER + File.separator + path + "."
				+ E3D_SCENE_EXTENSION);
	}

	public static String readFromFile(String path) {
		FileInputStream inFile = null;
		try {
			inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			return new String(str);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inFile != null)
				try {
					inFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public static byte[] readBytesFromImagesFolderFile(String path) {
		return readBytesFromFile(IMAGES_FOLDER + File.separator + path);
	}

	public static byte[] readBytesFromFile(String path) {
		FileInputStream inFile = null;
		try {
			inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			return str;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inFile != null)
				try {
					inFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public static BufferedImage readAndFlipTGAImage(String path) {
		return flipImage(readTGAImage(path));
	}

	private static BufferedImage flipImage(BufferedImage image) {
		AffineTransform at = new AffineTransform();
		at.concatenate(AffineTransform.getScaleInstance(1, -1));
		at.concatenate(AffineTransform.getTranslateInstance(0,
				-image.getHeight()));
		return createTransformed(image, at);
	}

	private static BufferedImage createTransformed(BufferedImage image,
			AffineTransform at) {
		BufferedImage newImage = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = newImage.createGraphics();
		g.transform(at);
		g.drawImage(image, 0, 0, null);
		g.dispose();
		return newImage;
	}

	public static BufferedImage readJPGImage(String path) {
		throw new UnsupportedOperationException("Unimplemented yet!");
		/*
		 * try { return TGALoader.loadTGA(readBytesFromImagesFolderFile(path));
		 * } catch (IOException e) { e.printStackTrace(); } return null;
		 */
	}

	public static BufferedImage readTGAImage(String path) {
		try {
			return TGALoader.loadTGA(readBytesFromImagesFolderFile(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Vec3f parseVec3fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Vec3f(Float.parseFloat(splitted[0]),
				Float.parseFloat(splitted[1]),
				splitted.length > 2 ? Float.parseFloat(splitted[2]) : 0);
	}

	public static Color3f parseColor3fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Color3f(Float.parseFloat(splitted[0]),
				Float.parseFloat(splitted[1]),
				splitted.length > 2 ? Float.parseFloat(splitted[2]) : 0);
	}

	public static Vec2f parseVec2fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Vec2f(Float.parseFloat(splitted[0]),
				Float.parseFloat(splitted[1]));
	}

	public static float redf(int rgb) {
		return ((float) (rgb >> redOffset & 0xff)) / 255f;
	}

	public static float greenf(int rgb) {
		return ((float) (rgb >> greenOffset & 0xff)) / 255f;
	}

	public static float bluef(int rgb) {
		return ((float) (rgb >> blueOffset & 0xff)) / 255f;
	}

	public static void saveScene(String serializedString, String path) {
		saveStringToFile(serializedString, PACKAGES_FOLDER + File.separator
				+ path + "." + E3D_SCENE_EXTENSION);
	}

	public static void saveStringToFile(String content, String path) {
		FileWriter writer;
		try {
			writer = new FileWriter(new File(path), false);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Matrix getRotXMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);

		matrix.values[0][0] = 1;
		matrix.values[0][1] = 0;
		matrix.values[0][2] = 0;
		matrix.values[1][0] = 0;
		matrix.values[1][1] = c;
		matrix.values[1][2] = -s;
		matrix.values[2][0] = 0;
		matrix.values[2][1] = s;
		matrix.values[2][2] = c;

		return matrix;
	}

	public static Matrix getRotYMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = c;
		matrix.values[0][1] = 0;
		matrix.values[0][2] = s;
		matrix.values[1][0] = 0;
		matrix.values[1][1] = 1;
		matrix.values[1][2] = 0;
		matrix.values[2][0] = -s;
		matrix.values[2][1] = 0;
		matrix.values[2][2] = c;

		return matrix;
	}

	public static Matrix getRotZMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = c;
		matrix.values[0][1] = -s;
		matrix.values[0][2] = 0;
		matrix.values[1][0] = s;
		matrix.values[1][1] = c;
		matrix.values[1][2] = 0;
		matrix.values[2][0] = 0;
		matrix.values[2][1] = 0;
		matrix.values[2][2] = 1;

		return matrix;
	}

	public static BufferedImage readAndFlipJPGImage(String path) {
		return flipImage(readJPGImage(path));
	}

	public static Matrix randomAxisRotateMatrix(Vec3f axis, float teta) {
		return randomAxisRotateMatrix(axis.x, axis.y, axis.z, teta);
	}

	/**
	 * @param x
	 *            , y, z - axis coords around which rotate will be produce
	 * @param teta
	 *            - rotation angle
	 * @return - rotation matrix
	 */
	public static Matrix randomAxisRotateMatrix(float x, float y, float z,
			float teta) {
		float ct = (float) Math.cos(teta);
		float st = (float) Math.sin(teta);
		float omct = 1 - ct;
		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = ct + omct * x * x;
		matrix.values[0][1] = omct * x * y - st * z;
		matrix.values[0][2] = omct * x * y + st * y;

		matrix.values[1][0] = omct * y * x + st * z;
		matrix.values[1][1] = ct + omct * y * y;
		matrix.values[1][2] = omct * y * z - st * x;

		matrix.values[2][0] = omct * z * x - st * y;
		matrix.values[2][1] = omct * z * y + st * x;
		matrix.values[2][2] = ct + omct * z * z;
		return matrix;
	}
}
