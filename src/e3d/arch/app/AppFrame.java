package e3d.arch.app;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class AppFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private AppPanel panel;

	private Config config;

	private Thread thread;

	private boolean isInitialized = false;

	private boolean threadRunned = true;

	private long lastUpdate = System.currentTimeMillis();

	public AppFrame(Config config) {
		this.config = config;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponentsToPane();
		setSize(config.getWidth(), config.getHeight());
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(screenSize.width - config.getWidth(), screenSize.height
				- config.getHeight());
		setLocation((screenSize.width - config.getWidth()) / 2,
				(screenSize.height - config.getHeight()) / 2);
		setVisible(true);

		addMouseMotionListener(new MouseMotionListener() {

			boolean isInit = false;

			int oldX = 0;

			int oldY = 0;

			@Override
			public void mouseMoved(MouseEvent e) {
				Point point = e.getPoint();
				if (isInit) {
					getPanel().sendMouseDiff(point.x - oldX, point.y - oldY);
					oldX = point.x;
					oldY = point.y;
				} else {
					oldX = point.x;
					oldY = point.y;
					isInit = true;
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
			}
		});

		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				getPanel().sendKey(e.getKeyCode());
			}
		});

		thread = new Thread() {
			@Override
			public void run() {
				while (threadRunned) {
					if (System.currentTimeMillis() - lastUpdate > config
							.getUpdateTime()) {
						AppFrame.this.tick();
						lastUpdate = System.currentTimeMillis();
					}
					repaint();
				}
			}
		};

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				threadRunned = false;
				try {
					thread.join();
				} catch (InterruptedException e1) {
				}
				System.exit(0);
			}
		});
	}

	protected void tick() {
		getPanel().tick();
	}

	protected void addComponentsToPane() {
		addComponentsToContentPane(getContentPane());
	}

	protected void addComponentsToContentPane(Container container) {
		container.add(getPanel(), BorderLayout.CENTER);
	}

	protected AppPanel getPanel() {
		if (panel == null) {
			panel = new AppPanel(config);
		}
		return panel;
	}

	@Override
	public void paint(Graphics g) {
		if (!isInitialized) {
			isInitialized = true;
			thread.start();
		}
		super.paint(g);
	}

}
