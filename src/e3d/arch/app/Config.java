package e3d.arch.app;

import e3d.arch.engine.console.DefaultConsoleProvider;
import e3d.arch.engine.console.IConsoleProvider;

public class Config {

	private int width;

	private int height;

	private int updateTime = 1000 / 70;

	private IConsoleProvider consoleProvider;
	
	private boolean isStats = true;

	public Config() {
		this(600, 600);
	}

	public Config(int width, int height) {
		this(new DefaultConsoleProvider(), width, height);
	}

	public Config(IConsoleProvider consoleProvider, int width, int height) {
		this.width = width;
		this.height = height;
		this.consoleProvider = consoleProvider;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public IConsoleProvider getConsoleProvider() {
		return consoleProvider;
	}

	public void setConsoleProvider(IConsoleProvider consoleProvider) {
		this.consoleProvider = consoleProvider;
	}

	public int getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(int updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isStats() {
		return isStats;
	}

}
