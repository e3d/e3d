package e3d.arch.app;

import e3d.arch.engine.console.LoadableConsoleProvider;

public class App {

	public static void main(String[] args) {
		new AppFrame(
				new Config(new LoadableConsoleProvider("wall"), 400, 400));
	}

}
