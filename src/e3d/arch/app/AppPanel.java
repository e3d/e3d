package e3d.arch.app;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import e3d.arch.engine.console.Console;

public class AppPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private int bufferWidth;
	private int bufferHeight;
	private Image bufferImage;
	private Graphics bufferGraphics;

	private Console console;

	public AppPanel(Config config) {
		console = config.getConsoleProvider().createConsole();
		console.setConfig(config);
	}

	public void paintBuffer(Graphics g) {
		if (g instanceof Graphics2D) {
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_STROKE_CONTROL,
					RenderingHints.VALUE_STROKE_NORMALIZE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_ALPHA_INTERPOLATION,
					RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_ENABLE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		}
		Rectangle bounds = getBounds();
		console.setSize(bounds.width, bounds.height);
		console.paint(g);
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

	private void resetBuffer() {
		bufferWidth = getSize().width;
		bufferHeight = getSize().height;
		if (bufferGraphics != null) {
			bufferGraphics.dispose();
			bufferGraphics = null;
		}
		if (bufferImage != null) {
			bufferImage.flush();
			bufferImage = null;
		}
		System.gc();
		bufferImage = createImage(bufferWidth, bufferHeight);
		bufferGraphics = bufferImage.getGraphics();
	}

	public void paint(Graphics g) {
		if (bufferWidth != getSize().width || bufferHeight != getSize().height
				|| bufferImage == null || bufferGraphics == null)
			resetBuffer();
		if (bufferGraphics != null) {
			bufferGraphics.clearRect(0, 0, bufferWidth, bufferHeight);
			paintBuffer(bufferGraphics);
			g.drawImage(bufferImage, 0, 0, this);
		}
	}

	public void sendKey(int key) {
		console.sendKey(key);
	}

	public void tick() {
		console.tick();
	}

	public void sendMouseDiff(int deltaX, int deltaY) {
		console.sendMouseMove(deltaX, deltaY);
	}

}
